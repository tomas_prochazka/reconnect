package reconnect

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path"
	"runtime"
	"strings"
)

const (
	LinuxAdbDownloadURL   = "https://dl.google.com/android/repository/platform-tools-latest-linux.zip"
	WindowsAdbDownloadURL = "https://dl.google.com/android/repository/platform-tools-latest-windows.zip"
	DarwinAdbDownloadURL  = "https://dl.google.com/android/repository/platform-tools-latest-darwin.zip"
)

func PlatformToolsLink() (string, error) {
	switch runtime.GOOS {
	case "linux":
		return LinuxAdbDownloadURL, nil
	case "windows":
		return WindowsAdbDownloadURL, nil
	case "darwin":
		return DarwinAdbDownloadURL, nil
	default:
		return "", fmt.Errorf("unsupported platform")
	}
}
func DownloadPlatformTools() error {
	url, err := PlatformToolsLink()
	if err != nil {
		return err
	}

	// create temp file
	tmpFile, err := ioutil.TempFile(os.TempDir(), "platform-tools-latest.zip")
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err := os.Remove(tmpFile.Name()); err != nil {
			panic(err)
		}
	}()
	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			panic(err)
		}
	}()
	// Write the body to file
	if _, err = io.Copy(tmpFile, resp.Body); err != nil {
		return err
	}
	if err := tmpFile.Close(); err != nil {
		return err
	}
	// open tmp file by unZipper
	if err := Unzip(tmpFile.Name(), os.TempDir()); err != nil {
		return err
	}

	return nil
}

func adbCommand() string {
	return path.Join(os.TempDir(), "platform-tools", "adb")
}
func fastbootCommand() string {
	return path.Join(os.TempDir(), "platform-tools", "fastboot")
}

func AdbCheck() error {
	cmd := exec.Command(adbCommand(), "version")

	var out, stdErr bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stdErr

	if err := cmd.Run(); err != nil {
		return fmt.Errorf("failed to run command: %+v: %+v", strings.TrimSpace(stdErr.String()), err)
	}

	_ = strings.TrimSpace(out.String())

	return nil
}

func KillAdbServer() {
	cmd := exec.Command(adbCommand(), "kill-server")

	var stdErr bytes.Buffer
	cmd.Stderr = &stdErr

	if err := cmd.Run(); err != nil {
		fmt.Printf("failed to run command: %+v: %+v\n", strings.TrimSpace(stdErr.String()), err)
	}
}

func UnlockBootloader() error {
	cmd := exec.Command(fastbootCommand(), "flashing", "unlock")

	var stdErr bytes.Buffer
	cmd.Stderr = &stdErr

	if err := cmd.Run(); err != nil {
		return fmt.Errorf("failed to run command: %+v: %+v", strings.TrimSpace(stdErr.String()), err)
	}

	return nil
}

func RebootToBootloader() error {
	cmd := exec.Command(adbCommand(), "reboot", "bootloader")

	var stdErr bytes.Buffer
	cmd.Stderr = &stdErr

	if err := cmd.Run(); err != nil {
		return fmt.Errorf("failed to run command: %+v: %+v", strings.TrimSpace(stdErr.String()), err)
	}

	return nil
}

func GetPublicIP() (string, error) {
	cmd := exec.Command(adbCommand(), "shell", "wget --no-check-certificate -q -O - https://api.ipify.org/")

	var out, stdErr bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stdErr

	if err := cmd.Run(); err != nil {
		return "", fmt.Errorf("failed to run command: %+v: %+v", strings.TrimSpace(stdErr.String()), err)
	}

	ip := strings.TrimSpace(out.String())

	return ip, nil
}

func SetMobileNetwork(enable bool) error {
	status := "disable"
	if enable {
		status = "enable"
	}

	cmd := exec.Command(adbCommand(), "shell",
		fmt.Sprintf("svc data %s", status))

	var stdErr bytes.Buffer
	cmd.Stderr = &stdErr

	if err := cmd.Run(); err != nil {
		return fmt.Errorf("failed to run command: %+v: %+v", strings.TrimSpace(stdErr.String()), err)
	}
	return nil
}
