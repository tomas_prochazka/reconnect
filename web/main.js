const vm = new Vue({
        el: '#app',
        data: {
            hello: 'Hello world',
            ipAddress: '0.0.0.0',
            toggleInterval: 5,
        },
        methods: {
            refreshIpAddress() {
                let self = this;

                self.ipAddress = '0.0.0.0';

                return axios.get("http://localhost:8080/ip-address")
                    .then(res => {
                        self.ipAddress = res.data
                    })
                    .catch(err => {
                        self.ipAddress = err;
                        if (err.response) {
                            self.ipAddress = err.response;
                            if (err.response.data) {
                                self.ipAddress = err.response.data;
                            }
                        }
                    });
            },
            toggleMobileData() {
                this.disableMobileData();
                return setTimeout(this.enableMobileData, this.toggleInterval*1000);
            },
            enableMobileData() {
                this.setMobileData(true);
            },
            disableMobileData() {
                this.setMobileData(false);
            },
            setMobileData(enable) {
                let self = this;
                return axios.put("http://localhost:8080/mobile-network", {enable}).then(res => {
                    self.ipAddress = '0.0.0.0'
                })
                    .catch(err => {
                        self.ipAddress = err.response.data;
                    }).then(() => setTimeout(this.refreshIpAddress, 3000));
            }
        },
        created() {
            this.refreshIpAddress();
        }
    })
;