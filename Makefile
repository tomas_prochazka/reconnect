GOOS = linux
GOARCH = amd64
BINDATAPACKAGE = reconnect
MAINPATH = cmd/main.go
BINARYNAME = reconnect

bindata:
	$(GOPATH)/bin/go-bindata-assetfs -pkg $(BINDATAPACKAGE) web/...

build: bindata
	go build -o $(BINARYNAME) $(MAINPATH)

run:
	./$(BINARYNAME)

all: build run
