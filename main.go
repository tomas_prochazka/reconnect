package reconnect

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/pkg/browser"
)

func ipAddressHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	ip, err := GetPublicIP()
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		if _, err := w.Write([]byte(err.Error())); err != nil {
			log.Println(err)
		}
		return
	}

	w.WriteHeader(http.StatusOK)
	if _, err := w.Write([]byte(ip)); err != nil {
		log.Println(err)
	}
}

func mobileNetworkHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPut {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	data := struct {
		Enable bool `json:"enable"`
	}{}

	if err := decoder.Decode(&data); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		if _, err := w.Write([]byte(err.Error())); err != nil {
			log.Println(err)
		}
		return

	}

	if err := SetMobileNetwork(data.Enable); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		if _, err := w.Write([]byte(err.Error())); err != nil {
			log.Println(err)
		}
		return
	}

	w.WriteHeader(http.StatusOK)
}

func adbCheckHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	if err := AdbCheck(); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		if _, err := w.Write([]byte(err.Error())); err != nil {
			log.Println(err)
		}
		return
	}

	w.WriteHeader(http.StatusOK)
}

func Main() error {
	if err := DownloadPlatformTools(); err != nil {
		return err
	}

	KillAdbServer()
	defer KillAdbServer()

	port := ":8080"

	http.HandleFunc("/adb-check", adbCheckHandler)
	http.HandleFunc("/ip-address", ipAddressHandler)
	http.HandleFunc("/mobile-network", mobileNetworkHandler)
	http.Handle("/", http.FileServer(assetFS()))

	url := fmt.Sprintf("http://localhost%s", port)
	if err := browser.OpenURL(url); err != nil {

	}

	if err := http.ListenAndServe(port, nil); err != nil {
		return err
	}
	return nil
}
