package main

import "reconnect"

func main() {
	if err := reconnect.Main(); err != nil {
		panic(err)
	}
}
